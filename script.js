const movieList = [
    {
      title: "Avatar",
      releaseYear: 2009,
      duration: 162,
      director: "James Cameron",
      actors: ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
      description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
      poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
      rating: 7.9,
    },
    {
      title: "300",
      releaseYear: 2006,
      duration: 117,
      director: "Zack Snyder",
      actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
      description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
      poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
      rating: 7.7,
    },
    {
      title: "The Avengers",
      releaseYear: 2012,
      duration: 143,
      director: "Joss Whedon",
      actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
      description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
      poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
      rating: 8.1,
    },
  ];  

  // Affichage de chaque film avec une boucle, avec toutes les informations qui les composent
for (const eachMovie of movieList) {
    console.log(`Title: ${eachMovie.title}`);
    console.log(`Release Year: ${eachMovie.releaseYear}`);
    console.log(`Duration: ${eachMovie.duration} minutes`);
    console.log(`Director: ${eachMovie.director}`);
    console.log(`Actors: ${eachMovie.actors.join(', ')}`);
    console.log(`Description: ${eachMovie.description}`);
    console.log(`Poster: ${eachMovie.poster}`);
    console.log(`Rating: ${eachMovie.rating}`);
    console.log('-----------------------');
  }

// Suppression d'un des films de la liste
const leastFavouriteMovie = movieList.shift();
console.log("Cet élément a été enlevé :", leastFavouriteMovie);


  // Ajout d'un film supplémentaire
  console.log("Ajout d'un film supplémentaire:");
const favouriteMovie = {
    title: "The Dark Knight",
    releaseYear: 2008,
    duration: 152,
    director: "Christopher Nolan",
    actors: ["Christian Bale", "Heath Ledger", "Gary Oldman"],
    description: "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.",
    poster: "https://fr.web.img2.acsta.net/medias/nmedia/18/63/97/89/18949761.jpg",
    rating: 9.1,
};
movieList.push(favouriteMovie);

// Affichage de la liste mise à jour en changeant de méthode (for et non pas for of pour m'entraîner aussi)
console.log("Liste mise à jour après l'ajout et la suppression:");
for (let i = 0; i < movieList.length; i++) {
  const movie = movieList[i];
  console.log(`Title: ${movie.title}`);
  console.log(`Release Year: ${movie.releaseYear}`);
  console.log(`Duration: ${movie.duration} minutes`);
  console.log(`Director: ${movie.director}`);
  console.log(`Actors: ${movie.actors.join(', ')}`);
  console.log(`Description: ${movie.description}`);
  console.log(`Poster: ${movie.poster}`);
  console.log(`Rating: ${movie.rating}`);
  console.log('-----------------------');
}
